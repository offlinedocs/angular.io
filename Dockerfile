# Build
FROM node:12 AS builder

RUN git clone https://github.com/angular/angular.git /angular

WORKDIR /angular/aio

RUN yarn
RUN yarn docs
RUN ls /angular/aio

# Serve
FROM nginx:alpine

COPY --from=builder /angular/aio/dist /usr/share/nginx/html
